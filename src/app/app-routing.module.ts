import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListCatsComponent } from './pages/list-cats/list-cats.component';
import { ViewCatComponent } from './pages/view-cat/view-cat.component';


const routes: Routes = [
  {
    path:'list-cats',
    component: ListCatsComponent,
  },
  {
    path:'view-cat/:id',
    component:ViewCatComponent,
  }
,{
    path:'**',
    component: ListCatsComponent,
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
