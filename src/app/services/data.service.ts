import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  url = "https://api.thecatapi.com/";

  constructor(private http:HttpClient) { }


/*   saveData(value:any){
    return this.http.post(this.url + 'save-datos', value);
  }

  getAllData(){
    return this.http.get(this.url + 'get-all-data')
  }

  deleteData(value:any){
    return this.http.delete(this.url + 'delete-data/' + value)
  }

  getDataById(value:any){
    return this.http.get(this.url + 'data-by-id/' + value)

  }

  login(value:any){
    return this.http.post(this.url + 'login', value)
  } */

  listarCats(value:any){
    return this.http.get(this.url + "v1/images/search?limit=" +  value.numero + "&api_key=live_UVF3Breyc5m2E0c42ZbjjXUA6wzogj3WQqeJAMcZtFlTMK7PL4kvTj4oWzsLEbZL")
  }

  listaByImage(value:any){
    return this.http.get(this.url + "v1/images/" +  value )
  }

 
  listarCountry(){
    return this.http.get( this.url + 'v1/breeds' )
    
  }

  filtrar(value:any){
    let origen = value.origin;
    let nombre = value.name;
    let categoria = value.categoria;
    let parametros = origen + ',' + nombre + ',' + categoria;
    return this.http.get( this.url + 'v1/images/search?breed_ids=' +  parametros)
  }

  viewCat(value:any){
    return this.http.get( this.url + 'v1/images/' + value)
  }

  listarCategories(){
    return this.http.get( this.url + 'v1/categories')
  }

}
