import { Component , OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-list-cats',
  templateUrl: './list-cats.component.html',
  styleUrls: ['./list-cats.component.css']
})
export class ListCatsComponent implements OnInit{

  checkoutForm!: FormGroup;
  data:any;
  parametro : any;
  listado:any;
  listadoImagenes: any;
  filterDetail:any;
  origins:any = [];
  names:any = [];
  dataFilter:any = [];
  categorias: any = [];

  constructor(private readonly fb: FormBuilder,
    private dataService:DataService,
    private rutaActiva: ActivatedRoute,
    private router: Router) {}

  ngOnInit():void{
    this.checkoutForm = this.initForm();
    this.parametro = this.rutaActiva.snapshot.params;
    console.log(this.parametro);

    
    this.dataService.listarCountry()
    .subscribe(resp=>{
      this.origins = resp;
      this.names = resp;
    })

    this.dataService.listarCategories()
    .subscribe(resp =>{
      this.categorias = resp;
      console.log(resp)
    })


  }

  onSubmit():void{

    this.listadoImagenes = [];

    console.log(this.checkoutForm.value)

    if(this.checkoutForm.value.numero > 0){
      this.dataService.listarCats(this.checkoutForm.value)
      .subscribe(resp =>{
          console.log(resp);
          this.listado = resp ;
          let i;
          for(i=0;i<= this.listado.length; i++){
            this.dataService.listaByImage(this.listado[i].id)
            .subscribe(resp2 =>{
                console.log(resp2)
  
                this.listadoImagenes.push(resp2);
            })
          }
      })
    }
    else{

      this.dataService.filtrar(this.checkoutForm.value)
        .subscribe(resp =>{
          console.log(resp)
          this.dataFilter = resp;
        })
    }
    


  }

  initForm(): FormGroup {
     return this.fb.group({
      numero:[''],
      origin:[''],
      name:[''],
      categoria:[''],
     })
  }

  verImages(value:any){
    console.log(value)

    this.router.navigate(['/view-cat/'+ value]);
  }



}
