import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-view-cat',
  templateUrl: './view-cat.component.html',
  styleUrls: ['./view-cat.component.css']
})
export class ViewCatComponent implements OnInit {

  parametro:any;
  viewCat: any;
  
  constructor(
    private dataService:DataService,
    private rutaActiva: ActivatedRoute,
    ) {}

  ngOnInit():void{

    this.parametro = this.rutaActiva.snapshot.params;
    console.log(this.parametro)
    this.dataService.viewCat(this.parametro.id)
    .subscribe(resp =>{
        this.viewCat = resp;
      console.log(resp)
    })

  }

}
